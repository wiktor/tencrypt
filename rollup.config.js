import htmlInsert from 'rollup-plugin-html-insert'
import commonjs from '@rollup/plugin-commonjs';
import static_files from 'rollup-plugin-static-files';
import { nodeResolve } from '@rollup/plugin-node-resolve'
import * as path from "path";

export default {
	input: 'src/main.js',
	output: {
		dir: 'dist',
		format: 'iife'
	},
    watch: {
        include: 'src/**'
    },
    external: [],
    plugins: [
        {
            name: 'watch-external',
            buildStart(){
                this.addWatchFile(path.resolve('src', 'index.html'))
            }
        },
        static_files({
            include: ['./static']
        }),
        commonjs(),
        nodeResolve({
            browser: true
        }),
        htmlInsert({
            template: './src/index.html'
        })
    ]
}