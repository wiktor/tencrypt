import * as openpgp from 'openpgp'
import HKP from '@openpgp/hkp-client'
import WKD from '@openpgp/wkd-client'

const re = /#(wkd|hkp);([^;]+)(?:;([^;]+))?/
const hash = window.location.hash

const appData = {
  /** @type {openpgp.PublicKey} */
  publicKey: undefined
}

if (hash && re.test(hash)) {
  notify('info', 'The tool is ready, encrypt a message below!')
  document.querySelector('#tool').classList.remove('hidden')

  document.querySelector('#encrypt_message').addEventListener('click', evt => {
    /** @type {HTMLTextAreaElement} */
    const el = document.querySelector('#message')
    const message = el.value
    run(message)
  })

  document.querySelector('#reset_message').addEventListener('click', evt => {
    notify('info', 'The tool is ready, encrypt a message below!')
    /** @type {HTMLTextAreaElement} */
    const elTextArea = document.querySelector('#message')
    elTextArea.value = ''
    /** @type {HTMLOutputElement} */
    const elOutput = document.querySelector('#tool output')
    elOutput.value = ''
  })
}

/**
 * @param {string} level
 * @param {string} message
 */
function notify (level, message) {
  /** @type {HTMLElement} */
  const el = document.querySelector('#notify')

  if (level && message) {
    el.innerHTML = message
    el.dataset.level = level
  } else {
    el.innerHTML = ''
    el.dataset.level = ''
  }
}

/**
 * @param {string} [message]
 */
async function run (message) {
  if (!message) { return }

  notify('log', 'Encrypting your message, please wait…')

  if (!re.test(hash)) { return }

  const [, protocol, id, extra] = hash.match(re)
  console.log(protocol, id, extra)

  /** @type {openpgp.PublicKey} */
  let publicKey
  /** @type {string} */
  let encrypted

  switch (protocol) {
    case 'wkd':
      try {
        publicKey = await fetchKeyWkd(id, extra)
      } catch (error) {
        notify('error', `Could not fetch key: ${error.message}`)
      }
      if (!publicKey) { return }
      try {
        encrypted = await encryptMessageOpenpgp(publicKey, message)
      } catch (error) {
        notify('error', `Could not encrypt message: ${error.message}`)
      }
      break

    case 'hkp':
      try {
        publicKey = await fetchKeyHkp(id, extra)
      } catch (error) {
        notify('error', `Could not fetch key: ${error.message}`)
      }
      if (!publicKey) { return }
      try {
        encrypted = await encryptMessageOpenpgp(publicKey, message)
      } catch (error) {
        notify('error', `Could not encrypt message: ${error.message}`)
      }
      break

    default:
      notify('error', `${protocol} is not a valid protocol`)
      break
  }

  if (!encrypted) { return }

  notify('success', 'Message encrypted, see result below!')
  document.querySelector('#tool output').textContent = encrypted
}

/**
 * @param {openpgp.PublicKey} publicKey
 * @param {string} text
 */
async function encryptMessageOpenpgp (publicKey, text) {
  const encrypted = await openpgp.encrypt({
    message: await openpgp.createMessage({ text }),
    encryptionKeys: publicKey
  })

  return `${encrypted}`
}

/**
 * @param {string} id
 * @param {string} extra
 */
async function fetchKeyWkd (id, extra) {
  if (appData.publicKey) {
    return appData.publicKey
  }

  const wkd = new WKD()
  const publicKeyBytes = await wkd.lookup({
    email: id
  })

  if (!publicKeyBytes) {
    throw new Error('no such key found on the server')
  }

  const publicKey = await openpgp.readKey({
    binaryKey: publicKeyBytes
  })

  appData.publicKey = publicKey
  return publicKey
}

/**
 * @param {string} id
 * @param {string} extra
 */
async function fetchKeyHkp (id, extra) {
  if (appData.publicKey) {
    return appData.publicKey
  }

  const hkp = new HKP(extra ? `https://${extra}` : 'https://keys.openpgp.org')
  const publicKeyArmored = await hkp.lookup({
    query: id
  })

  if (!publicKeyArmored) {
    throw new Error('no such key found on the server')
  }

  const publicKey = await openpgp.readKey({
    armoredKey: publicKeyArmored
  })

  appData.publicKey = publicKey
  return publicKey
}
